# Final-Project-Sanbercode

# FINAL PROJECT

Tugas ini adalah tugas FINAL PROJECT SANBERCODE REACT NATIVE, aplikasi ini saya beri nama IMDB

# Tentang Aplikasi

Aplikasi ini berisi tentang kumpulan movies atau film yang dikemas secara rapi di dalam aplikasi ini. Di dalam aplikasi ini hanya menampilkan FIlm Harry Potter saja, dan juga berikut keterangannya.


# API YANG DIGUNAKAN

Aplikasi ini menggunakan API dari omdb API, yakni http://www.omdbapi.com/?apikey=39398370&s="Harry Potter"

# CATATAN

Aplikasi ini saya buat secara individu, masih banyak kekurangan dalam aplikasi ini mohon dimaklumi. Semoga kedepan saya dapat meningkatkan skill saya di bidang React Native ini.

# LINK COMMIT

https://gitlab.com/apiiRR/final-project-sanbercode/-/commit/9ccf8d70481cbad51552957e137073f61cb02ac9
