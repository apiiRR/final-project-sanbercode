import SignIn from './components/SignIn';
import SignUp from './components/SignUp';
import About from './components/About';
import Home from './components/Home';
import React from "react";
import {NavigationContainer, DrawerActions} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';



const AboutStack = createStackNavigator();
const AboutStackScreen = () => (
    <AboutStack.Navigator screenOptions={{headerShown: false}}>
        <AboutStack.Screen name='About' component={About} />
    </AboutStack.Navigator>
)

const Beranda = ({route}) => (
    <Drawer.Navigator>
            <Drawer.Screen name='Home' component={Home} />
            <Drawer.Screen name='About' component={AboutStackScreen} />
            <Drawer.Screen name='Logout' component={SignIn} />
    </Drawer.Navigator>
)

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

export default () => (
    <NavigationContainer>
        <Stack.Navigator screenOptions={{headerShown: false}} initialRouteName="SignIn">
            <Stack.Screen name="SignIn" component={SignIn} />
            <Stack.Screen name="Beranda" component={Beranda} />
            <Stack.Screen name="SignUp" component={SignUp} />
        {/* <Drawer.Navigator>
            <Drawer.Screen name='Home' component={TabScreen} />
            <Drawer.Screen name='About' component={AboutStackScreen} />
        </Drawer.Navigator> */}
        </Stack.Navigator>
    </NavigationContainer>
)
