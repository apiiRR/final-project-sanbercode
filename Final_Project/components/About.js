import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput, ScrollView, Button} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';


export default class About extends Component {
    render(){
        return(
            <View style={styles.container}>
                <ScrollView contentContainerStyle={{flexGrow: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={styles.title}>About Me</Text>
                    <View style={{flexDirection: 'row', marginTop: 72}}>
                        <Image source={require('./Images/profile.jpg')} style={{width: 100, height: 100, borderRadius: 100}} />
                        <View style={{marginLeft: 30}}>
                            <Text style={{color: 'white', fontSize: 28, fontWeight: 'bold'}}>Rafi Ramadhana</Text>
                            <Text style={{color: 'white', fontSize: 20, fontWeight: 'normal'}}>Full Stack Developer</Text>
                        </View>
                    </View>
                    <View style={styles.about}>
                        <Text style={styles.aboutText}>I have 1 years commercial exprerience providing front-end development, producing responsive mobile app and  exceptional user experience</Text>
                    </View>
                    <Text style={{color: 'white', marginTop: 72, fontSize: 28, lineHeight: 30, fontWeight: 'bold'}}>Experience  <Text style={{color: 'red'}}>────────</Text></Text>
                    <View style={{flexDirection: 'row', marginTop: 50}}>
                        <View>
                            <Icon name='react' color='white' size={70} />
                            <Text style={{color: 'white', textAlign: 'center'}}>React Native</Text>
                        </View>
                        <View style={{marginLeft: 50, marginRight: 50}}>
                            <Icon name='git' color='white' size={70} />
                            <Text style={{color: 'white', textAlign: 'center'}}>Git</Text>
                        </View>
                        <View>
                            <Icon name='laravel' color='white' size={70} />
                            <Text style={{color: 'white', textAlign: 'center'}}>Laravel</Text>
                        </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 30, marginBottom: 50}}>
                        <View>
                            <Icon name='gitlab' color='white' size={70} />
                            <Text style={{color: 'white', textAlign: 'center'}}>React Native</Text>
                        </View>
                        <View style={{marginLeft: 50, marginRight: 50}}>
                            <Icon name='language-javascript' color='white' size={70} />
                            <Text style={{color: 'white', textAlign: 'center'}}>Git</Text>
                        </View>
                        <View>
                            <Icon name='language-python' color='white' size={70} />
                            <Text style={{color: 'white', textAlign: 'center'}}>Laravel</Text>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container : {
        backgroundColor: 'black'
    },
    title : {
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 32,
        lineHeight: 37,
        marginTop: 49
    },
    about: {
        marginTop: 44,
        width: 320,
    },
    aboutText : {
        color: 'white',
        fontStyle: 'italic',
        fontSize: 16,
        lineHeight: 20
    }
})