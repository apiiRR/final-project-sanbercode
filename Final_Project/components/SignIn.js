import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput, ScrollView, Button} from 'react-native';
import {Link} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/Foundation';

export default class LoginScreen extends Component {
    render() {
        return (
            <View style={styles.container} >
                <ScrollView contentContainerStyle={{flexGrow: 1, justifyContent: 'center', alignItems: 'center'}}>
                    <View style={styles.turboTron}>
                        <Image source={require('./Images/logo.png')} style={{width: 297, height: 270}} />
                    </View>
                    <View style={styles.form} >
                        <Text style={styles.lable}>Username / Email</Text>
                        <TextInput style={styles.textInput} />
                        <Text style={styles.lable}>Password</Text>
                        <TextInput style={styles.textInput} />
                    </View>
                    <TouchableOpacity style={styles.masuk} onPress={() => this.props.navigation.navigate('Beranda')}>
                        <Text style={{fontSize: 18, lineHeight: 28, textAlign: 'center', color: '#FFFFFF', fontWeight: 'bold'}}>Sign In</Text>
                    </TouchableOpacity>
                    <Text style={{marginTop: 50, textAlign: 'center', fontWeight: 'bold', color: '#FFFFFF', fontSize: 13, lineHeight: 20}}> ───────────  OR SIGN IN WITH  ─────────── </Text>
                    <View style={styles.icons}>
                        <Icon name='social-facebook' size={50} color='white'></Icon>
                        <Icon style={{marginRight: 34, marginLeft: 34}} name='social-twitter' size={50} color='white'></Icon>
                        <Icon name='social-google-plus' size={50} color='white'></Icon>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: 34, marginBottom: 50}}>
                        <Text style={{color: 'white', fontSize: 15, marginRight: 15}}>Don't have account?</Text>
                        <Text style={{color: 'white', fontSize: 15, borderBottomWidth:1, borderBottomColor: 'red'}} onPress={() => this.props.navigation.navigate('SignUp')}>Sign Up</Text>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        fontFamily: 'Roboto',
        alignItems: 'center',
        backgroundColor: 'black',
    },
    turboTron: {
        marginTop: 101
    },
    form : {
        marginTop: 31,
        marginBottom: 50
    },
    lable: {
        color: '#FFFFFF',
        fontSize: 13,
        lineHeight: 20,
        marginBottom: 5
    },
    textInput: {
        height: 44, 
        backgroundColor: '#F7F8F9', 
        borderWidth: 1, 
        borderColor: '#D5DDE0',
        marginBottom: 16,
        width: 348,
        borderRadius: 15,
    },
    masuk : {
        width: 348,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#FF0000',
        borderRadius: 15,
    },
    icons : {
        flexDirection: 'row',
        marginTop: 30
    }
});