import React, {Component} from 'react';
import {StyleSheet, Text, View, Image, TouchableOpacity, TextInput, ScrollView, Button, FlatList, ActivityIndicator} from 'react-native';
import Axios from 'axios';

export default class Home extends Component {

    constructor(props){
        super(props);
        this.state = {
            data: {},
            isLoading: true,
            isError: false
        };
    }

    componentDidMount(){
        this.getData();
    }

    getData = async () => {
        try{
            const response = await Axios.get('http://www.omdbapi.com/?apikey=39398370&s=Harry Potter')
            this.setState({isError: false, isLoading: false, data: response.data.Search})
        } catch (error) {
            this.setState({isLoading: false, isError: true})
        }
    }

    render() {
        if(this.state.isLoading){
            return(
                <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
                    <ActivityIndicator size='large' color='red' />
                </View>
            )
        }

        else if (this.state.isError) {
            <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
                <Text>Data Error</Text>
            </View>
        }

        return (
            <View style={styles.container}>
                <Text style={styles.title}>Harry Potter Movies</Text>
                <View style={{borderColor: 'red', borderWidth: 3, width: 350, borderRadius: 30, marginTop: 16}} ></View>
                <FlatList 
                data={this.state.data}
                renderItem={({item}) =>
                <TouchableOpacity onPress={() => this.props.navigator.push({index: 1, passProps:{imdbID: item.imdbID} })}>
                <View style={styles.viewList}>
                    <Image source={{uri: `${item.Poster}`}} style={styles.image} />
                    <Text style={styles.itemTitle}>{item.Title}</Text>
                </View> 
                </TouchableOpacity> } 
                keyExtractor={({imdbID}, index) => index }/>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        flexGrow: 1, 
        alignItems: 'center'
    },
    title: {
        color: 'white',
        marginTop: 50,
        fontSize: 32,
        fontWeight: 'bold'
    },
    image: {
        width: 310,
        height: 466,
        borderRadius: 50,
        borderWidth: 1,
        borderColor: 'white',
        marginTop: 30
    },
    viewList: {
        alignItems: 'center'
    },
    itemTitle: {
        textAlign: 'center',
        color: 'white',
        fontWeight: 'bold',
        fontSize: 25,
        fontStyle: 'italic',
        marginTop: 10
    }
})